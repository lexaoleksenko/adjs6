let btn = document.querySelector(".btn")
let divGlob = document.querySelector(".glob")

async function knowIp(){
    const responseIp = await (await fetch("https://api.ipify.org/?format=json")).json()

    var endpoint = `http://ip-api.com/json/${responseIp.ip}?fields=status,message,continent,countryCode,country,regionName,city,lat,lon`;

    const responseJson = await (await fetch(endpoint)).json()

    let div1 = document.createElement("div")
    div1.textContent = `${responseJson.continent}`
    divGlob.append(div1)
    let div2 = document.createElement("div")
    div2.textContent = `${responseJson.country}`
    divGlob.append(div2)
    let div3 = document.createElement("div")
    div3.textContent = `${responseJson.countryCode}`
    divGlob.append(div3)
    let div4 = document.createElement("div")
    div4.textContent = `${responseJson.city}`
    divGlob.append(div4)
    let div5 = document.createElement("div")
    div5.textContent = `${responseJson.regionName}`
    divGlob.append(div5)
    console.log(responseJson)
}

btn.addEventListener("click", knowIp)
